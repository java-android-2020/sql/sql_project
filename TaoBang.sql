create database quanlydatban;
drop database quanlydatban;
use quanlydatban; -- Chuyển qua sử dụng quá lý đặt bàn bình thường mặc định sẽ là master

drop table tbl_deposit;
drop table tbl_table;
drop table tbl_customer;
drop table tbl_bankaccount;
drop table tbl_points;
drop table tbl_bookinginfo;
drop table tbl_booking;
drop table tbl_bill;
drop table tbl_billinfo;
drop table tbl_food;

CREATE TABLE tbl_deposit
(
	DepositID int auto_increment primary key,
    Deposit long not null,
    TotalPersons int not null
);


CREATE TABLE tbl_table
(
	TableID int auto_increment primary key,
    TableName NVARCHAR(20) not null,
    Seat int not null,
    TableStatus NVARCHAR(20) not null
);

CREATE TABLE tbl_point
(
	PointID int auto_increment primary key,
    Price long not null,
    Percent int not null
);

CREATE TABLE tbl_customer
(
      CustomerID  INT auto_increment PRIMARY KEY,
      Email NVARCHAR(40) not null unique,
	  PhoneNumber VARCHAR(15) NOT NULL,
      FullName NVARCHAR(30) not null,
      PassWord varchar(15) not null,
      TotalMoney int not null
);


CREATE TABLE tbl_bankaccount
(
   BankAccountID int auto_increment primary key,
   AccountNo NVARCHAR(20) not null,
   Balance long NOT NULL,
   Status int not null,
   Email NVARCHAR(40) not null,
   constraint fk_BankaccountCustomer foreign key (Email) references tbl_customer(Email)
);

CREATE TABLE tbl_booking
(
	BookingID int auto_increment primary key,
    CustomerID int not null,
    Deposit long not null,
    BookingTime datetime not null,
    TotalSeats int not null,
	DepositID int,
    constraint fk_BookingDeposit foreign key (DepositID) references tbl_deposit(DepositID),
    constraint fk_BookingCustomer foreign key (CustomerID) references tbl_customer(CustomerID)
);

CREATE TABLE tbl_bookinginfo
(
	BookingInfoID int auto_increment primary key,
    BookingID int not null,
    TableID int not null, 
    constraint fk_BookinginfoBooking foreign key (BookingID) references tbl_booking(BookingID),
    constraint fk_BookinginfoTable foreign key (TableID) references tbl_table(TableID)
);

create table tbl_bill
(
	BillID int auto_increment primary key,
    PointID int not null,
    BookingID int,
    Discount int not null,
    Amount long not null,
    constraint fk_BillPoints foreign key (PointID) references tbl_point(PointID),
    constraint fk_BillBooking foreign key (BookingID) references tbl_booking(BookingID)
);

create table tbl_food
(
	FoodID int auto_increment primary key,
    FoodName varchar(30) not null,
    FoodPrice long not null
);

create table tbl_billinfo
(
	BillInfoID int auto_increment primary key,
    BillID int,
    Quantity int,
    FoodID int,
    constraint fk_BillInfoBill foreign key (BillID) references tbl_bill(BillID),
    constraint fk_BillInfoFood foreign key (FoodID) references tbl_food(FoodID)
);










